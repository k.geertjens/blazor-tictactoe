using Blazor_TicTacToe.Data;
using FluentAssertions;

namespace Blazor_TicTacToe_Tests
{
    public class BoardTests
    {
        BoardData board;

        public BoardTests()
        {
           board = new BoardData();
        }

        [Fact]
        public void TestStartingState()
        {
            // The game needs to start with an empty board and winner, currentPlayer as X, and gameEnded as false
            board.currentPlayer.Should().Be(TileValue.X);
            board.gameEnded.Should().Be(false);
            board.winner.Should().Be(TileValue.Empty);

            for(int row = 0; row < BoardData.boardSize; row++)
            {
                for(int col = 0; col < BoardData.boardSize; col++)
                {
                    board.board[row, col].Should().Be(TileValue.Empty);
                }
            }
        }

        [Fact]
        public void TestResetGame()
        {
            // ResetGame needs to clear the board and winner, set the starting player as X, and set gameEnded as false

            // Make some changes to test if reset works (this should make X the winner)
            (int, int)[] moves = new (int, int)[] { (0, 0), (1, 0), (0, 1), (1, 1), (0, 2) };
            foreach ((int, int) tile in moves)
            {
                board.SelectTile(tile.Item1, tile.Item2);
            }

            // Reset and test if all values are as expected
            board.ResetGame();

            board.currentPlayer.Should().Be(TileValue.X);
            board.gameEnded.Should().Be(false);
            board.winner.Should().Be(TileValue.Empty);

            for (int row = 0; row < BoardData.boardSize; row++)
            {
                for (int col = 0; col < BoardData.boardSize; col++)
                {
                    board.board[row, col].Should().Be(TileValue.Empty);
                }
            }


        }

        [Fact]
        public void TestSelectTile()
        {
            // A tile's value should change to the currentPlayer's value when SelectTile is called
            board.board[0, 0].Should().Be(TileValue.Empty);
            board.board[0, 1].Should().Be(TileValue.Empty);

            board.SelectTile(0, 0);
            board.board[0, 0].Should().Be(TileValue.X);

            board.SelectTile(0, 1);
            board.board[0, 1].Should().Be(TileValue.O);
        }

        [Fact]
        public void TestInvalidSelectTile()
        {
            // If SelectTile is called on a tile that's already filled in, nothing should happen
            board.board[0, 0].Should().Be(TileValue.Empty);

            board.SelectTile(0, 0);
            board.board[0, 0].Should().Be(TileValue.X);

            board.SelectTile(0, 0);
            board.board[0, 0].Should().Be(TileValue.X);
        }

        [Fact]
        public void TestTogglePlayer()
        {
            // After a tile is selected, the currentPlayer needs to be toggled between X and O
            board.currentPlayer.Should().Be(TileValue.X);
            board.SelectTile(0, 0);
            board.currentPlayer.Should().Be(TileValue.O);
            board.SelectTile(1, 0);
            board.currentPlayer.Should().Be(TileValue.X);
        }

        [Fact]
        public void TestWin()
        {
            // The game should end and select the correct winner when a winning combination is played
            board.gameEnded.Should().Be(false);
            board.winner.Should().Be(TileValue.Empty);

            // Check for winner after some moves without a winner or a draw yet
            (int, int)[] moves = { (0, 0), (0, 1) };
            foreach ((int, int) tile in moves)
            {
                board.SelectTile(tile.Item1, tile.Item2);
            }

            board.gameEnded.Should().Be(false);
            board.winner.Should().Be(TileValue.Empty);

            // Play a winning combination for X (horizontal row)
            board.ResetGame();
            moves = new (int, int)[] { (0, 0), (1, 0), (0, 1), (1, 1), (0, 2) };
            foreach ((int, int) tile in moves)
            {
                board.SelectTile(tile.Item1, tile.Item2);
            }

            board.gameEnded.Should().Be(true);
            board.winner.Should().Be(TileValue.X);

            // Play a winning combination for O (vertical column)
            board.ResetGame();
            moves = new (int, int)[] { (0, 1), (0, 0), (0, 2), (1, 0), (1, 2), (2, 0) };
            foreach ((int, int) tile in moves)
            {
                board.SelectTile(tile.Item1, tile.Item2);
            }

            board.gameEnded.Should().Be(true);
            board.winner.Should().Be(TileValue.O);

            // Play a winning combination for X (top-left -> bottom-right diagonal)
            board.ResetGame();
            moves = new (int, int)[] { (0, 0), (2, 0), (1, 1), (1, 0), (2, 2) };
            foreach ((int, int) tile in moves)
            {
                board.SelectTile(tile.Item1, tile.Item2);
            }

            board.gameEnded.Should().Be(true);
            board.winner.Should().Be(TileValue.X);

            // Play a winning combination for X (bottom-left -> top-right diagonal)
            board.ResetGame();
            moves = new (int, int)[] { (2, 0), (0, 0), (1, 1), (1, 0), (0, 2) };
            foreach ((int, int) tile in moves)
            {
                board.SelectTile(tile.Item1, tile.Item2);
            }

            board.gameEnded.Should().Be(true);
            board.winner.Should().Be(TileValue.X);
        }

        [Fact]
        public void TestDraw()
        {
            // The game should end and set the winner as Empty when all tiles are filled in without a winning condition
            board.gameEnded.Should().Be(false);
            board.winner.Should().Be(TileValue.Empty);

            (int, int)[] moves = new (int, int)[] { (0, 0), (1, 1), (0, 2), (0, 1), (1, 0), (2, 0), (2, 1), (1, 2), (2, 2) };
            foreach ((int, int) tile in moves)
            {
                board.SelectTile(tile.Item1, tile.Item2);
            }

            board.gameEnded.Should().Be(true);
            board.winner.Should().Be(TileValue.Empty);
        }
    }
}