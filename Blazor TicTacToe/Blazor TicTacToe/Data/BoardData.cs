﻿using System.Reflection;

namespace Blazor_TicTacToe.Data
{
    public class BoardData
    {
        public const int boardSize = 3;

        public TileValue currentPlayer { get; private set; } // Value that the selected tile will be set to for the current player
        public TileValue winner { get; private set; } // Winning player, or TileValue.Empty if the game was a draw
        public bool gameEnded { get; private set; } // Whether the game has ended, either on a win or draw
        public TileValue[,] board { get; private set; } = new TileValue[boardSize, boardSize]; // The board state is stored in this 2D array, containing values from the TileValue enum (Empty, X, O)

        public BoardData()
        {
            ResetGame();
        }

        public void SelectTile(int row = 0, int col = 0)
        {
            // Assign the cell at the given row/column the value of currentPlayer
            if (!gameEnded && board[row, col] == TileValue.Empty)
            {
                board[row, col] = currentPlayer;

                // Check if anyone has won, or if game was a draw, stop executing this function if true
                if (CheckWin())
                {
                    // A player has filled in a row successfully
                    gameEnded = true;
                    winner = currentPlayer;
                    return;
                }

                else if (CheckDraw())
                {
                    // All tiles are filled in (if CheckWin did not return true, that means game was a draw)
                    gameEnded = true;
                    return;
                }

                // If no win or draw, switch to next player
                TogglePlayer();
            }
        }

        public void TogglePlayer()
        {
            // Toggle currentPlayer between X and O
            if (currentPlayer == TileValue.X)
            {
                currentPlayer = TileValue.O;
            }
            else
            {
                currentPlayer = TileValue.X;
            }
        }

        private bool CheckWin()
        {
            // Checks if a player has won and return either true or false
            // No need to return the winning player, since that will be the currentPlayer (if CheckWin is called between SelectTile and TogglePlayer)

            for (int i = 0; i < boardSize; i++)
            {
                // Check current row
                bool rowEqual = true;
                for (int j = 0; j < boardSize; j++)
                {
                    if (board[i, 0] != TileValue.Empty && board[i, 0] == board[i, j]) continue;
                    else
                    {
                        rowEqual = false;
                        break;
                    }
                }

                // Check current column
                bool colEqual = true;
                for (int j = 0; j < boardSize; j++)
                {
                    if (board[0, i] != TileValue.Empty && board[0, i] == board[j, i]) continue;
                    else
                    {
                        colEqual = false;
                        break;
                    }
                }

                if (rowEqual || colEqual) return true; // Return true if either row or column was all equal
            }

            // Top-left -> bottom-right diagonal
            bool diagonal1Equal = true;
            for (int i = 0; i < boardSize; i++)
            {
                if (board[0, 0] != TileValue.Empty && board[0, 0] == board[i, i]) continue;
                else
                {
                    diagonal1Equal = false;
                    break;
                }
            }

            // Bottom-left -> top-right diagonal
            bool diagonal2Equal = true;
            for (int i = 0; i < boardSize; i++)
            {
                if (board[boardSize - 1, 0] != TileValue.Empty && board[boardSize - 1, 0] == board[boardSize - 1 - i, i]) continue;
                else
                {
                    diagonal2Equal = false;
                    break;
                }
            }

            if (diagonal1Equal || diagonal2Equal) return true; // Return true if either diagonal was all equal

            return false; // Return false if none of the win conditions were true
        }

        private bool CheckDraw()
        {
            // Returns true if all tiles are filled in (used after CheckWin to determine if game was a draw)
            foreach (TileValue val in board)
            {
                if (val == TileValue.Empty) return false;
            }

            return true;
        }

        public void ResetGame()
        {
            // Reset the game state
            currentPlayer = TileValue.X;
            gameEnded = false;
            winner = TileValue.Empty;

            // Clear board
            for (int row = 0; row < boardSize; row++)
            {
                for (int col = 0; col < boardSize; col++)
                {
                    board[row, col] = TileValue.Empty;
                }
            }
        }
    }
}
