﻿namespace Blazor_TicTacToe.Data
{
    public enum TileValue
    {
        Empty,
        X,
        O
    }
}
