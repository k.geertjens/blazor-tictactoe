# Blazor TicTacToe

This is a basic Tic-Tac-Toe game created with Blazor. I created this game as a first small project to get to know Blazor in a fun way, as well as learn about unit testing with xUnit and FluentAssertions.

## Building and Running

You can either open this solution in Visual Studio to build and run it, or use the .NET SDK in the command line to build: https://learn.microsoft.com/en-us/dotnet/core/sdk

Run the following code from the root directory in the command line to build all projects in the solution:
```
cd "./Blazor TicTacToe"
dotnet publish -c Release
cd "./bin/Release/net7.0/publish/"
"Blazor TicTacToe.exe"
```

This will build a release version of the application, and run the server. You can now navigate to `localhost:5000` in your browser to see the application.

## Running Unit Tests
Unit tests are executed every time a commit is made using the CI/CD pipeline. You can also manually run them through Visual Studio, or via the command line using the .NET SDK: https://learn.microsoft.com/en-us/dotnet/core/sdk

Run the following code from the root directory to run the unit tests:
```
cd "./Blazor TicTacToe/Blazor TicTacToe Tests"
dotnet test
```